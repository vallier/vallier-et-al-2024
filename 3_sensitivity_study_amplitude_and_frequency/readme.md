Here are the codes: 
- **diffusion2D_comp_article.ipynb** in the main repository 
- **diffusion2D.ipynb** in the 'augmentation_debit', 'augmentation_freq' and 'osc_default' folders

## Description: 

In each folder, the code diffusion2D.ipynb is undertaking the numerical solving of the diffusion equation of overpressures due to an exploitation with an oscillating flow rate. Each folder corresponds to a study case: 
1. for 'augmentation_debit', a monopole with an oscillating flow rate of 240 m3/h 
2. for 'augmentation_freq', a monopole with an oscillating flow rate with a frequency of 1/12 1/h. 
3. for 'osc_default', a monopole with an oscillating flow rate of 120 m3/h with a frequency of 1/24 1/h. 

## Outputs:

For each solving of the diffusion equation there are the outputs saved in the 'save_profiles' subfolder:
1. Data files named 'Pt_xxm_inj_cst.dat' of different temporal (time in hours in 1st column) evolution of overpressures (in bars in 2nd column) for injection with an oscillating flow rate. Here xx is equal to different tested set (x,y) coordinates: {xx=200m for {200m,200m} ; xx=500m for {500m,500m} ; xx=1km for {1km,1km} ; xx=2km for {2km,2km} ; xx=4km for {4km,4km} ; xx=6km for {6km,6km} and xx=8km for {8km,8km}}.
2. Data files named 'Px_xxm_inj_cst.dat' of spatial profiles (distance in km in 1st column) of overpressures (in bars in 2nd column) for injection with an oscillating flow rate. Here xx is equal to different tested set y-coordinate: {xx=200m for {200m,200m} ; xx=500m for {500m,500m} ; xx=1km for {1km,1km} ; xx=2km for {2km,2km} ; xx=4km for {4km,4km} ; xx=6km for {6km,6km} and xx=8km for {8km,8km}}.

In the main repository, diffusion2D_comp_article.ipynb uses as inputs all these data files in order to display the eight following figures:
1. Spatial profiles of far-field overpressures due to an injection with an oscillating flow rate with an amplitude of 120 m3/h and doubled, of 240 m3/h
2. Spatial profiles of far-field overpressures due to an injection with an oscillating flow rate with a frequency of 1/24 1/h and doubled, of 1/12 1/h
3. Spatial profiles of close-field overpressures due to an injection with an oscillating flow rate with an amplitude of 120 m3/h and doubled, of 240 m3/h 
4. Spatial profiles of close-field overpressures due to an injection with an oscillating flow rate with a frequency of 1/24 1/h and doubled, of 1/12 1/h
5. Temporal evolutions of far-field overpressures due to an injection with an oscillating flow rate with a frequency of 1/24 1/h and doubled, of 1/12 1/h
6. Temporal evolutions of far-field overpressures due to an injection with an oscillating flow rate with an amplitude of 120 m3/h and doubled, of 240 m3/h
7. Temporal evolutions of close-field overpressures due to an injection with an oscillating flow rate with a frequency of 1/24 1/h and doubled, of 1/12 1/h
8. Temporal evolutions of close-field overpressures due to an injection with an oscillating flow rate with an amplitude of 120 m3/h and doubled, of 240 m3/h

For more information, please read the article of Vallier et al. (2024) entitled '_Alternative pumping strategies to stimulate a reservoir without triggering far-field faults_'

## How to run the program:

- Install Python 3.9.13
- In your Terminal in /augmentation_debit, /augmentation_freq and /osc_default run ipython: ipython diffusion2D.ipynb
- Then in your Terminal run ipython: ipython gen_figs_results.ipynb
Or execute directly on Jupyter Notebook or equivalent