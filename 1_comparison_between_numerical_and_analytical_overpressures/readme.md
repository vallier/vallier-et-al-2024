Here is the code **diffusion2D_green_fc_monopole-dipole-fit-parfait.ipynb**

## Description: 

the code is undertaking a comparison between the numerical far-field pressure disturbances and the analytical ones that have been deduced from the Green's solution of the diffusion equation. The comparison is conducted for the monopole and the dipole. 

## Outputs:


1. Map of pressure disturbances for the monopole

2. Comparison of the analytical and numerical spatial profiles of the far-field pressure disturbances along the x-axis for the monopole 

3. Map of pressure disturbances for the dipole

4. Comparison of the analytical and numerical spatial profiles of the far-field pressure disturbances along the x-axis for the dipole 

For more information, please read the article of Vallier et al. (2024) entitled '_Alternative pumping strategies to stimulate a reservoir without triggering far-field faults_'

## How to run the program:

Install Python 3.9.13
In your Terminal run ipython: ipython diffusion2D_green_fc_monopole-dipole-fit-parfait.ipynb
Or execute directly on Jupyter Notebook or equivalent
