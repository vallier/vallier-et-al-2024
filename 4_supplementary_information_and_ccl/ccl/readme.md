Here are the codes: 
- **gen_figure_ccl.ipynb** in the main repository 
- **diffusion_monop_cst.ipynb** in the 'monop_cst' folder 
- **diffusion_monop_osc.ipynb** in the 'monop_osc' folder 
- **diffusion_monop_osc_freq_2.ipynb** in the 'monop_osc_freq_2' folder

## Description: 

In the subfolders, the codes diffusion_monop_cst.ipynb, diffusion_monop_osc.ipynb and diffusion_monop_osc_frq_2.ipynb are undertaking respectively the numerical solving of the diffusion equation of overpressures due to an exploitation for three pumping strategies: 
1. Monopole with a constant flow rate 
2. Monopole with an oscillating flow rate with a frequency of 1/24 1/h 
3. Monopole with an oscillating flow rate with a frequency of 1/12 1/h. 

## Outputs:

- For each code in the subfolders, a data file named 'Pmax_r.dat' with the maximum of overpressure (2nd column, in bars) with its corresponding x coordinate (1st column, in km) along a x-axis going through the monopole.

- In the main repository, gen_figure_ccl.ipynb uses as inputs all these data files in order to display: the spatial profiles of maximum of pressure disturbances in function of the distance from the injection for the three study cases

For more information, please read the article of Vallier et al. (2024) entitled '_Alternative pumping strategies to stimulate a reservoir without triggering far-field faults_'

## How to run the program:

- Install Python 3.9.13
- In your Terminal in /monop_cst run ipython: ipython diffusion_monop_cst.ipynb, /monop_osc run ipython: ipython diffusion_monop_osc.ipynb, /monop_osc_freq_2 run ipython: ipython diffusion_monop_osc_freq_2.ipynb Or execute them on Jupyter Notebook or equivalent
- Then in your Terminal run ipython: gen_figure_ccl.ipynb
Or execute directly on Jupyter Notebook or equivalent