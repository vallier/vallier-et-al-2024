Here are the codes: 
- **gen_figs_results.ipynb** in the main repository
- **diffusion_monop_cst.ipynb** in the 'monop_cst' folder 
- **diffusion_dip_cst.ipynb** in the 'dip_cst' folder
- **diffusion_monop_osc.ipynb** in the 'monop_osc' folder

## Description: 

In the subfolders, the codes diffusion_monop_cst.ipynb, diffusion_dip_cst.ipynb and diffusion_monop_osc.ipynb are undertaking respectively the numerical solving of the diffusion equation of overpressures due to an exploitation for three pumping strategies: 
1. Monopole with a constant flow rate  
2. Dipole with a constant flow rate
3. Monopole with an oscillating flow rate. 

## Outputs:

For each solving of the diffusion equation there are the outputs saved in the 'save_profiles' subfolder:
1. A data file named 'Pmax_r.dat' with the maximum of overpressure (2nd column, in bars) with its corresponding x coordinate (1st column, in km) along a x-axis going through the monopole or dipole
2. Data files named 'taux_Pmax_vs_Vi_r_xx.dat' of the ratios of the close-well maximum pressure disturbance over the distant one in function of ratio of distance (2nd column) with its corresponding injected total volume (1st column). Here xx is equal to different tested distance ratios: {xx=01 for 0.1 ; xx=008 for 0.08 ; xx=006 for 0.06 ; xx=004 for 0.04}.
3. Only for diffusion_monop_osc.ipynb, an equivalent data file ''taux_Pmax_vs_f_r_xx.dat'' is generated: the ratios of the close-well maximum pressure disturbance over the distant one are in function of ratio of distance (2nd column) with its corresponding frequency (1st column).

In the main repository, gen_figs_results.ipynb uses as inputs all these data files in order to display the two following figures:
1. Spatial profiles of maximum of pressure disturbances in function of the distance
from the injection for the three study cases versus the minimum of the assumed activation pressure
2. Ratio of the close-well maximum pressure disturbance over the distant one in
function of ratio of distance for all pumping strategies

For more information, please read the article of Vallier et al. (2024) entitled '_Alternative pumping strategies to stimulate a reservoir without triggering far-field faults_'

## How to run the program:

- Install Python 3.9.13
- In your Terminal in /monop_cst run ipython: ipython diffusion_monop_cst.ipynb, /dip_cst run ipython: ipython diffusion_dip_cst.ipynb, /monop_osc run ipython: ipython diffusion_monop_osc.ipynb Or execute them on Jupyter Notebook or equivalent
- Then in your Terminal run ipython: ipython gen_figs_results.ipynb
Or execute directly on Jupyter Notebook or equivalent